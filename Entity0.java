/* GWU CS6431 Computer Networks Chenghu He G45573591 */

public class Entity0 extends Entity
{    
    // Perform any necessary initialization in the constructor
    public Entity0()
    {
    	int m = distanceTable.length;
    	int n = distanceTable[0].length;
    	for (int i = 0; i < m; ++i) {
    		minCost[i] = 999;
    		distanceTable[i][0] = NetworkSimulator.cost[i][0];
    	}
    	for (int i = 0; i < m; ++i) {
    		for (int j = 1; j < n; ++j) {
    			distanceTable[i][j] = 999;
    		}
    	}
    	printDT();
    	broadcast();
    }
    
    private void broadcast() {
    	boolean hasUpdate = false;
    	int m = distanceTable.length;
    	int n = distanceTable[0].length;
    	for (int i = 0; i < m; ++i) {
    		int iMin = 999;
    		for (int j = 0; j < n; ++j) {
    			iMin = Math.min(iMin, distanceTable[i][j]);
    		}
    		if (iMin != minCost[i]) {
    			hasUpdate = true;
    			minCost[i] = iMin;
    		}
    	}
    	if (!hasUpdate) {
    		return;
    	}
    	Packet p1 = new Packet(0, 1, minCost);
    	Packet p2 = new Packet(0, 2, minCost);
    	Packet p3 = new Packet(0, 3, minCost);
    	NetworkSimulator.toLayer2(p1);
    	NetworkSimulator.toLayer2(p2);
    	NetworkSimulator.toLayer2(p3);
    }
    
    // Handle updates when a packet is received.  Students will need to call
    // NetworkSimulator.toLayer2() with new packets based upon what they
    // send to update.  Be careful to construct the source and destination of
    // the packet correctly.  Read the warning in NetworkSimulator.java for more
    // details.
    public void update(Packet p)
    {
    	if (p.getDest() != 0) {
    		System.out.println("Bad update dest");
    		return;
    	}
    	int s = p.getSource();
    	int m = distanceTable.length;
    	int ctos = distanceTable[s][0];
    	for (int i = 0; i < m; ++i) {
    		int ncos = ctos + p.getMincost(i);
    		distanceTable[i][s] = ncos;
    	}
    	printDT();
    	broadcast();
    }
    
    public void linkCostChangeHandler(int whichLink, int newCost)
    {
    	int m = distanceTable.length;
    	int diff = newCost - distanceTable[whichLink][0];
    	distanceTable[whichLink][0] = newCost;
    	for (int i = 0; i < m; ++i) {
    		distanceTable[i][whichLink] += diff;
    	}
    	printDT();
    	broadcast();
    }
    
    public void printDT()
    {
        System.out.println();
        System.out.println("           via");
        System.out.println(" D0 |   1   2   3");
        System.out.println("----+------------");
        for (int i = 1; i < NetworkSimulator.NUMENTITIES; i++)
        {
            System.out.print("   " + i + "|");
            for (int j = 1; j < NetworkSimulator.NUMENTITIES; j++)
            {
                if (distanceTable[i][j] < 10)
                {    
                    System.out.print("   ");
                }
                else if (distanceTable[i][j] < 100)
                {
                    System.out.print("  ");
                }
                else 
                {
                    System.out.print(" ");
                }
                
                System.out.print(distanceTable[i][j]);
            }
            System.out.println();
        }
    }
}
