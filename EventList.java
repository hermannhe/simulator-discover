/* GWU CS6431 Computer Networks Chenghu He G45573591 */

public interface EventList
{
    public boolean add(Event e);
    public Event removeNext();
    public String toString();
    public double getLastPacketTime(int entityFrom, int entityTo);
}
